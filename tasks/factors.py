def factors(number):
    list = []

    for x in range(number):
        if x != 0 and (number % x) == 0 and x != 1:
            list.append(x)
    return list


print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
