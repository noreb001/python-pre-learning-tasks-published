def calculator(a, b, operator):
    if operator == "+":
        sum = a + b
    elif operator == '-':
        sum = a - b
    elif operator == '*':
        sum = a * b
    else:
        sum = a / b

    sum = int(sum)
    sum = bin(sum)
    sum = sum[2:]

    return sum


print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
